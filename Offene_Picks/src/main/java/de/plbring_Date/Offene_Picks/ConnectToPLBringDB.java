package de.plbring_Date.Offene_Picks;


import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.DriverManager;
	import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;



import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


	public class ConnectToPLBringDB {
	
	HSSFWorkbook workbook = new HSSFWorkbook();
	String fileName = new SimpleDateFormat("yyyyMMddHHmm'.xls'").format(new Date());
	public static void main(String[] args) {
		
		ConnectToPLBringDB run =new ConnectToPLBringDB();
		System.out.println("Start");
		
		run.getDateFromPL_DB_Tab_Gestelle();
		//run.getDateFromPL_DB_Tab_Offene_Picks();
		//run.getDateFromPL_DB_Tab_Mitarbeiter();
	}

	
	public void getDateFromPL_DB_Tab_Gestelle(){
		try{  
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection conToPL= DriverManager.getConnection(  
			"jdbc:oracle:thin:@wms-db.abholmeister.de:1521:PLSTORE","PLBRING_MU_UPD","bluvirFtXaZHrcZ");  

			//step3 create the statement object  
			Statement stmt=conToPL.createStatement();  
			//Hier ist der SQL Statement
			String query ="SELECT  k.GESTELLNR, gp.BEZEICHNUNG, b.PALNR, wk.LADEFOLGE,  wk.TOUR, gp.AUFTRAGSNR, k.KO_GRUPPE\r\n" + 
					"FROM GESTELL_KOPF k\r\n" + 
					"JOIN GESTELL_POS gp\r\n" + 
					"ON k.GESTELLNR= gp.GESTELLNR\r\n" + 
					"JOIN WA_KOPF wk\r\n" + 
					"ON wk.AUFTRAGSNR=gp.AUFTRAGSNR\r\n" + 
					"JOIN BESTAND b\r\n" + 
					"ON b.PLATZ= gp.PLATZ -- and  p.PLATZ_ID=.PLATZ\r\n" + 
					"JOIN PLATZ p ON gp.PLATZ=p.PLATZ_ID\r\n" + 
					"WHERE p.PLATZ_ART ='KG' AND p.IST_LHM  LIKE '1' AND k.TYP='KG' \r\n" + 
					"ORDER BY  k.GESTELLNR, wk.LADEFOLGE ASC,  gp.BEZEICHNUNG";
			
			//step4 execute query  
			ResultSet rs=stmt.executeQuery(query);  
			
			
			    HSSFSheet sheet = workbook.createSheet("Komm-Gestelle");
			    HSSFRow rowhead = sheet.createRow((short) 0);
			    rowhead.createCell((short) 0).setCellValue("GESTELLNR");
			    rowhead.createCell((short) 1).setCellValue("BEZEICHNUNG");
			    rowhead.createCell((short) 2).setCellValue("PALNR");
			    rowhead.createCell((short) 3).setCellValue("LADEFOLGE");
			    rowhead.createCell((short) 4).setCellValue("TOUR");
			    rowhead.createCell((short) 5).setCellValue("AUFTRAGSNR");
			    rowhead.createCell((short) 6).setCellValue("KO_GRUPPE");
			    
			    int i = 1;
			    
			  while (rs.next()){
				 // rs = stmt.getGeneratedKeys();
				  //rs.getInt(1);
			        HSSFRow row = sheet.createRow((short) i);
			        row.createCell((short) 0).setCellValue(rs.getString("GESTELLNR"));
			        row.createCell((short) 1).setCellValue(rs.getString("BEZEICHNUNG"));
			        row.createCell((short) 2).setCellValue(rs.getString("PALNR"));
			        row.createCell((short) 3).setCellValue(rs.getString("LADEFOLGE"));
			        row.createCell((short) 4).setCellValue(rs.getString("TOUR"));
			        row.createCell((short) 5).setCellValue(rs.getString("AUFTRAGSNR"));
			        row.createCell((short) 6).setCellValue(rs.getString("KO_GRUPPE"));
			        i++;
			    }
			    
			  // Path anpassen 
			    String yemi = System.getProperty("user.home") + "\\Desktop\\WA_Gestelle\\Daten_"+fileName;
			    FileOutputStream fileOut = new FileOutputStream(yemi);
			    workbook.write(fileOut);
			    fileOut.close();
			    conToPL.close();   
			    } catch (ClassNotFoundException e1) {
			       e1.printStackTrace();
			    } catch (SQLException e1) {
			        e1.printStackTrace();
			    } catch (FileNotFoundException e1) {
			        e1.printStackTrace();
			    } catch (IOException e1) {
			        e1.printStackTrace();
			    	
			    }
			
			
			System.out.println("Finish");
			
			//step5 close the connection object  
		 
		/*	tBase = new TestBase();
			tBase.updateProperty("aktuelleKommGestell", gestell);*/
	//		return gestell
	
	
		}
	
	
	
	
		public void getDateFromPL_DB_Tab_Offene_Picks(){
		try{  
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection conToPL= DriverManager.getConnection(  
			"jdbc:oracle:thin:@wms.cpcvqfcsai3r.eu-central-1.rds.amazonaws.com:1521:PLSTORE","plbring_ro","xpRhHqddhPPkVBa");  

			//step3 create the statement object  
			Statement stmt=conToPL.createStatement();  
			//Hier ist der SQL Statement
			String query ="SELECT bj.DATUM,  bj.USR, p.PERS_NAME, al.WE_STRAT, ws.BEZ, SUM(bj.MENGE_GESAMT) AS Summe_in_STK\r\n" + 
					"FROM BESTJOUR bj\r\n" + 
					"JOIN ARTIKEL_LAG al\r\n" + 
					"ON bj.ARTIKELNR = al.ARTIKELNR\r\n" + 
					"JOIN WE_STRATEG ws\r\n" + 
					"ON ws.TYP = al.WE_STRAT\r\n" + 
					"JOIN PERSONAL p\r\n" + 
					"ON p.KURZ_BEZ=bj.USR\r\n" + 
					"WHERE bj.BELEG1 ='Direkt-WE'\r\n" + 
					"GROUP BY  bj.DATUM,  bj.USR, p.PERS_NAME, al.WE_STRAT, ws.BEZ\r\n" + 
					"ORDER BY  bj.DATUM DESC, bj.USR, p.PERS_NAME, al.WE_STRAT, ws.BEZ";
			
			//step4 execute query  
			ResultSet rs=stmt.executeQuery(query);  
			
			
			    HSSFSheet sheet = workbook.createSheet("Verräumung");
			    HSSFRow rowhead = sheet.createRow((short) 0);
			    rowhead.createCell((short) 0).setCellValue("DATUM");
			    rowhead.createCell((short) 1).setCellValue("USR");
			    rowhead.createCell((short) 2).setCellValue("PERS_NAME");
			    rowhead.createCell((short) 3).setCellValue("WE_STRAT");
			    rowhead.createCell((short) 4).setCellValue("BEZ");
			    rowhead.createCell((short) 5).setCellValue("Summe_in_STK");
			    
			    int i = 1;
			    
			  while (rs.next()){
				 // rs = stmt.getGeneratedKeys();
				  //rs.getInt(1);
			        HSSFRow row = sheet.createRow((short) i);
			        row.createCell((short) 0).setCellValue(rs.getString("DATUM"));
			        row.createCell((short) 1).setCellValue(rs.getString("USR"));
			        row.createCell((short) 2).setCellValue(rs.getString("PERS_NAME"));
			        row.createCell((short) 3).setCellValue(rs.getString("WE_STRAT"));
			        row.createCell((short) 4).setCellValue(rs.getString("BEZ"));
			        row.createCell((short) 5).setCellValue(rs.getString("Summe_in_STK"));
			        i++;
			    }
			    
			  // Path anpassen 
			    String yemi = System.getProperty("user.home") + "\\Desktop\\WE_Verräumung\\Daten_"+fileName;
			    FileOutputStream fileOut = new FileOutputStream(yemi);
			    workbook.write(fileOut);
			    fileOut.close();
			    conToPL.close();   
			    } catch (ClassNotFoundException e1) {
			       e1.printStackTrace();
			    } catch (SQLException e1) {
			        e1.printStackTrace();
			    } catch (FileNotFoundException e1) {
			        e1.printStackTrace();
			    } catch (IOException e1) {
			        e1.printStackTrace();
			    	
			    }
			
			
			System.out.println("Finish");
			
			//step5 close the connection object  
		 
		/*	tBase = new TestBase();
			tBase.updateProperty("aktuelleKommGestell", gestell);*/
	//		return gestell
	
	
		}
		
		
		public void getDateFromPL_DB_Tab_Mitarbeiter() {
		try{  
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection conToPL= DriverManager.getConnection(  
			"jdbc:oracle:thin:@wms.cpcvqfcsai3r.eu-central-1.rds.amazonaws.com:1521:PLSTORE","plbring_ro","xpRhHqddhPPkVBa");  

			//step3 create the statement object  
			Statement stmt=conToPL.createStatement();  
			//Hier ist der SQL Statement

			String query ="SELECT  p.PERS_NAME, p.PERSNR, fp1.KO_GRUPPE, kg.BEZ AS Bezeichnung, fp2.Letzter_Pick, fp2.Seit_in_Min,  fp2.Status\r\n" + 
					"  ,to_char( SYSTIMESTAMP+ 2/24, 'DD.MM.YYYY HH24:MI:SS' ) AS Aufruf\r\n" + 
					"  FROM  FAHR_POS fp1\r\n" + 
					"  JOIN PERSONAL p\r\n" + 
					"    ON p.KURZ_BEZ = fp1.KURZ_NAME\r\n" + 
					"  JOIN KOMM_GRUPPE kg\r\n" + 
					"  ON fp1.KO_GRUPPE=kg.KO_GRUPPE\r\n" + 
					"  INNER JOIN (\r\n" + 
					"        SELECT p.PERS_NAME, p.PERSNR AS Pers_NR,  MAX(fp.ANFAHR_ZEIT) AS Letzter_Pick\r\n" + 
					"        ,  round((CAST(to_timestamp((to_char( localtimestamp, 'DD.MM.YYYY HH24:MI:SS' )), 'DD.MM.YYYY HH24:MI:SS') AS DATE) -  CAST(to_timestamp((concat(to_char(MAX(fp.ANFAHR_DATUM),'DD.MM.YYYY ' ), MAX(fp.ANFAHR_ZEIT))),  'DD.MM.YYYY HH24:MI:SS'  ) AS DATE))* 24 * 60 )   AS Seit_in_Min\r\n" + 
					"        , (case when (round((CAST(to_timestamp((to_char( localtimestamp, 'DD.MM.YYYY HH24:MI:SS' )), 'DD.MM.YYYY HH24:MI:SS') AS DATE) -  CAST(to_timestamp((concat(to_char(MAX(fp.ANFAHR_DATUM),'DD.MM.YYYY ' ), MAX(fp.ANFAHR_ZEIT))),  'DD.MM.YYYY HH24:MI:SS'  ) AS DATE))* 24 * 60 ))<10 then 'Aktiv' else 'Nicht aktiv' end) AS Status\r\n" + 
					"        FROM FAHR_POS fp\r\n" + 
					"        JOIN PERSONAL p\r\n" + 
					"        ON p.KURZ_BEZ = fp.KURZ_NAME\r\n" + 
					"        WHERE fp.MDENR IS NOT NULL AND fp.ANFAHR_DATUM LIKE TO_CHAR(sysdate, 'DD.MM.YY')\r\n" + 
					"        GROUP BY p.PERS_NAME, p.PERSNR) fp2\r\n" + 
					"    ON p.PERSNR=fp2.Pers_NR and fp1.ANFAHR_ZEIT=fp2.Letzter_Pick\r\n" + 
					"    ORDER BY fp2.Seit_in_Min";
			
			//step4 execute query  
			ResultSet rs=stmt.executeQuery(query);  
			
			
			    HSSFSheet sheet = workbook.createSheet("Mitarbeiter");
			    HSSFRow rowhead = sheet.createRow((short) 0);
			    rowhead.createCell((short) 0).setCellValue("PERS_NAME");
			    rowhead.createCell((short) 1).setCellValue("PERSNR");
			    rowhead.createCell((short) 2).setCellValue("KO_GRUPPE");
			    rowhead.createCell((short) 3).setCellValue("Bezeichnung");
			    rowhead.createCell((short) 4).setCellValue("Letzter_Pick");
			    rowhead.createCell((short) 5).setCellValue("Seit_in_Min");
			    rowhead.createCell((short) 6).setCellValue("Status");
			    rowhead.createCell((short) 7).setCellValue("Aufruf");
			    int i = 1;
			    
			  while (rs.next()){
				 // rs = stmt.getGeneratedKeys();
				  //rs.getInt(1);
			        HSSFRow row = sheet.createRow((short) i);
			        row.createCell((short) 0).setCellValue(rs.getString("PERS_NAME"));
			        row.createCell((short) 1).setCellValue(rs.getString("PERSNR"));
			        row.createCell((short) 2).setCellValue(rs.getString("KO_GRUPPE"));
			        row.createCell((short) 3).setCellValue(rs.getString("Bezeichnung"));
			        row.createCell((short) 4).setCellValue(rs.getString("Letzter_Pick"));
			        row.createCell((short) 5).setCellValue(rs.getString("Seit_in_Min"));
			        row.createCell((short) 6).setCellValue(rs.getString("Status"));
			        row.createCell((short) 7).setCellValue(rs.getString("Aufruf"));
			        i++;
			    }
			    
			  
			    String yemi = System.getProperty("user.home") + "\\Desktop\\Offene_Picks\\Daten_"+fileName;
			    FileOutputStream fileOut = new FileOutputStream(yemi);
			    workbook.write(fileOut);
			    fileOut.close();
			    conToPL.close();   
			    } catch (ClassNotFoundException e1) {
			       e1.printStackTrace();
			    } catch (SQLException e1) {
			        e1.printStackTrace();
			    } catch (FileNotFoundException e1) {
			        e1.printStackTrace();
			    } catch (IOException e1) {
			        e1.printStackTrace();
			    	
			    }
			
			
			System.out.println("Finish:");
			
			//step5 close the connection object  
		 
		/*	tBase = new TestBase();
			tBase.updateProperty("aktuelleKommGestell", gestell);*/
	//		return gestell

			
			
		// 
		
		
		
	
		}
	
	
	
}
