package de.bring.pl_mobil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;
import java.util.Random;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
 

public class ConnectToDB {
private String gestell;
//private List <String> gestellListe;
private String PID;

private String tourenGestell;
private String db_Username;
private String db_Passwort;



public static void main(String[] args) {
	ConnectToDB test =new ConnectToDB();
	System.out.println("KG Test "+test.getLeerKommGestellVonDB("BER"));
	System.out.println("TG  "+ test.getLeerTourenGestellVonDB("BER"));  
	
	
	
	System.out.println("How to encrypt Data:"+encrypt(readProperty("database.user_MUC.name")));
}
//readProperty("");

public static int getRandom(int from, int to) {
    if (from < to)
        return from + new Random().nextInt(Math.abs(to - from));
    return from - new Random().nextInt(Math.abs(to - from));
}


public void updateProperty(String config, String value) throws ConfigurationException {

	PropertiesConfiguration conf;
	
	conf = new PropertiesConfiguration(
			System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
	conf.setProperty(config, value);
	conf.save();

}

public static String decrypt(String encryptedPassword) {
	String decryptedPassword;
	byte[] decryptedPasswordBytes = Base64.getDecoder().decode(encryptedPassword);
	decryptedPassword = new String(decryptedPasswordBytes);
	return decryptedPassword;
	}
public static String encrypt(String password) {
	String encryptedPassword;
	byte[] encodedBytes = Base64.getEncoder().encode(password.getBytes());
	encryptedPassword = new String(encodedBytes);
	return encryptedPassword;
	}

public static String readProperty(String str) {
	String prop;
	PropertiesConfiguration conf;

	try {
		conf = new PropertiesConfiguration(
				System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
		prop = conf.getProperty(str).toString();
		conf.save();
		return prop;
	} catch (ConfigurationException e) {
		e.printStackTrace();
		return "leer";
	}
}

// TODO Auto-generated catch block

	public String getLeerKommGestellVonDB(String local){
	try{  //readProperty("URL_BER"),
		
		String host = decrypt(readProperty("database.host"));
		String port = decrypt(readProperty("database.port"));
		String db_Name = decrypt(readProperty("database.name"));
		if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
		}if(local.contains("MUC")) {
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
			}
		//step1 load the driver class  
		Class.forName(  "oracle.jdbc.driver.OracleDriver");  

		//step2 create  the connection object  
		
		java.sql.Connection con= DriverManager.getConnection(  
		"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

		//step3 create the statement object  
		Statement stmt=con.createStatement();  

		//step4 execute query  
			
		ResultSet rs=stmt.executeQuery("SELECT  * FROM (   SELECT  gk.GESTELLNR \r\n" + 
				"					FROM GESTELL_POS gp JOIN GESTELL_KOPF gk ON gp.GESTELLNR= gk.GESTELLNR\r\n" + 
				"					WHERE gk.TYP='KG' AND gk.GESTELLNR NOT IN ( \r\n" + 
				"                                            SELECT k.GESTELLNR  FROM GESTELL_KOPF k   \r\n" + 
				"                                            WHERE k.AKT_PLATZ  LIKE '001BC%'\r\n" + 
				"                                           )\r\n" + 
				"                    order by dbms_random.value )  WHERE ROWNUM=1 ");  
		while(rs.next())  
			gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

		//step5 close the connection object  
		con.close();  
	/*	tBase = new TestBase();
		tBase.updateProperty("aktuelleKommGestell", gestell);*/
		return gestell;

		}catch(Exception e){ System.out.println(e);}  
			return "Keine Komm-Gestell";
		}  
	// 
	
	public String getLeerTourenGestellVonDB(String local){
		try{  
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}	
			
			
			
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);    

			//step3 create the statement object  
			Statement stmt=con.createStatement();  
			//getRandom(1001, 9999);
			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT  gk.GESTELLNR \r\n" + 
					"FROM GESTELL_POS gp JOIN GESTELL_KOPF gk ON gp.GESTELLNR= gk.GESTELLNR\r\n" + 
					"WHERE gk.TYP='TG' AND gk.GESTELLNR NOT IN ( SELECT k.GESTELLNR  FROM GESTELL_KOPF k  \r\n" + 
					"WHERE k.AKT_PLATZ  LIKE '001TR%' OR gk.AKT_PLATZ  LIKE '001MO%') AND ROWNUM=1");  
			while(rs.next())  
				gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return gestell;

			}catch(Exception e){ System.out.println(e);}  
				return "Keine";
			}  

	public String getBereitsBelegtenTourenGestellVonDB(String kommGestell, String tourBez, String local){
		try{  
			System.out.println("Komm-Gestell ist "+kommGestell);
			
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));

			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}			
			
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  
			//gestellListe =  new ArrayList<String>();
			//step4 execute query  
			ResultSet rs=stmt.executeQuery(" SELECT DISTINCT vgp.GESTELLNR \r\n" + 
					" FROM V_GESTELL_POS vgp \r\n" + 
					" LEFT JOIN GESTELL_POS gp\r\n" + 
					" ON vgp.PLATZ=gp.PLATZ \r\n" + 
					" INNER JOIN GESTELL_KOPF gk\r\n" + 
					" ON gk.GESTELLNR=gp.GESTELLNR AND vgp.GESTELLNR=gk.GESTELLNR\r\n" + 
					" WHERE gk.TYP = 'TG' AND vgp.TOURNR IN (SELECT DISTINCT vgp1.TOURNR FROM V_GESTELL_POS vgp1 WHERE vgp1.Gestellnr = '"+kommGestell+"' AND vgp1.TOURBEZ = '"+tourBez+"')");  
			if(rs.next()){  
				//int i =0;
				 tourenGestell = rs.getString(1); //+","+rs.getString(2)+","+rs.getString(3)+","+rs.getString(3);
				
			}
			
			    System.out.println("Komm-Gestelle "+kommGestell+", Tourbezeichung: "+tourBez);
				System.out.println("Tour-Gestelle "+tourenGestell);
			
			//step5 close the connection object  
			con.close();  
			return tourenGestell;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  

	// Diese Methode gibt anhand des Komm-Gestellfaches der PID zurück 
	public String getPIDvomKommGestellVonDB(String gestellfach, String local){
		try{  
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
			{
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND b WHERE b.PLATZ='001"+gestellfach+"' AND ROWNUM=1");  
			while(rs.next())  
				this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.PID;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  
	
	// Diese Methode gibt anhand des Quell-Platzes die LHMNr zurück 
	public String getLHMNrvomQuellPlatzVonDB(String quellPlatz, String local){
		try{  
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND b WHERE b.PLATZ='001"+quellPlatz+"' AND ROWNUM=1");  
			while(rs.next())  
				this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.PID;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			} 
	
}
