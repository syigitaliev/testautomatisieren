package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;

public class MenueWaage extends Base{

	// Initialisierung von Locators 
	@FindBy(how= How.CLASS_NAME, using = "paramwinzig")
	private WebElement maske_ID_Locator;
	
	@FindBy(how=How.XPATH, using ="//div[@id='uebersicht']/table")
	private WebElement menueTable;
		
//____________________________________________________________________________________________
	// Definition der Konstruktors  
	public MenueWaage(WebDriver driver) {
		super(driver);
	}
//____________________________________________________________________________________________
	// Definition von  Methoden
	@Override
	public boolean checkMaskeID(int  ID){
		return checkMaske(maske_ID_Locator, ID, 3);
	}
	
	public void clickMenueOption( String text){
		if(checkMaskeID(200)){
		String tmp ="//input[@class='menuebutton' and @value ='"+text+"']";
		WebElement menueOptions= menueTable.findElement(By.xpath(tmp));
		click(menueOptions, 3);
		}
	}
}