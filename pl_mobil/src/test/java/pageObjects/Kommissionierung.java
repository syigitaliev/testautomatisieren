package pageObjects;


import org.apache.commons.configuration.ConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;
import de.bring.pl_mobil.ConnectToDB;
import de.bring.pl_mobil.TestBase;



public class Kommissionierung extends Base {
	String kommZone;
	String[] gestellArtikelMengePlatz = new String[10];
	String gestell;
	String artikel;
	String menge;
	String lagerplatz;
	TestBase tBase;
	ConnectToDB conToDB;
	String standort ;
	String maske ="0";
	String nextMaske = "";
	int index=0;
	// Initialisierung von Locators

	@FindBy(how = How.CLASS_NAME, using = "paramwinzig")
	private WebElement restMengen_Locator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td")
	private WebElement KommZoneLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\'uebersicht\']/table/tbody/tr[1]/td")
	private WebElement KommZoneGETLocator;

	@FindBy(how = How.ID, using = "eingabe1")
	private WebElement inputTextLocator;

	@FindBy(how = How.XPATH, using = "//*[@class='ftaste_vor' and @value='Ok']")
	private WebElement buttonOKLokator;

	//@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[1]/td[2]")
//	private WebElement gestellfachLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td[2]")
	private WebElement gestellfachFuerTueteLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[2]/td[2]")
	private WebElement artikelNrLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[5]/td[2]")
	private WebElement lagerplatzLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td[2]")
	private WebElement mengenLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[3]/td[2]")
	private WebElement supermarktLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='steuerung']/table/tbody/tr/td[2]/input")
	private WebElement weiterButtonLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@class='wert']")
	private WebElement tuetenPIDOrGestellLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@class = 'wertgross' and @colspan='3']")
	private WebElement bedienthekeQuellPlatzLokator;
	
	@FindBy(how = How.XPATH, using = "//td[(text()= 'Zielfach: ')]/following-sibling::td")
	private WebElement zielGestellfachLokator;
	
	@FindBy(how = How.XPATH, using = "//td[(text()= 'Gestell: ')]/following-sibling::td")
	private WebElement zielGestellLokator;
	


	// ____________________________________________________________________________________________
	// Definition der Konstruktors
	public Kommissionierung(WebDriver driver) throws Exception {
		super(driver);
		tBase = new TestBase();
		conToDB = new ConnectToDB();
		standort =readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort" );
	}
	// ____________________________________________________________________________________________
	// Definition von Methoden


	public void kommPlatzBesetzt() {
	// MaskenID Korrigieren
		if(checkMaskeID(4200)) {
			clickOnOK();
			
		}
		
	}
	
	

	public void setKommZone() throws ConfigurationException {
		// Prüfe, ob keine Fahraufträge mehr gibt
		
		
		if (checkMaskeID(4200)) {
			System.out.println("Die Zone von der Property: " + tBase.readProperty("aktuelleKommZone"));
			kommZone = KommZoneLocator.getText();
			tBase.updateProperty("aktuelleKommZone", kommZone);
			//System.out.println(tBase.readProperty("aktuelleKommZone nach dem Update im Property"));
		}
		else if (checkMaskeID(4217)) {
			System.out.println("Die Zone von der Property: " + tBase.readProperty("aktuelleKommZone"));
			kommZone = KommZoneGETLocator.getText();
			tBase.updateProperty("aktuelleKommZone", kommZone);
			//System.out.println(tBase.readProperty("aktuelleKommZone nach dem Update im Property"));
		}

		
	}

	public String getKommZone() {
		//System.out.println(tBase.readProperty("aktuelleKommZone"));
		return tBase.readProperty("aktuelleKommZone");
	}

	public void inputText(String text) {

		// System.out.println(kommZone);
		type(inputTextLocator, 3, text);
	}

	public void clickOnOK() {
		
		 click(buttonOKLokator, 5);

	}

	// Rückgabe Gestellfach
	public String getKommGestellfach() {
		
		String tmp = "";
		if(checkMaskeID(4262)) {
			
			
			tmp =tuetenPIDOrGestellLocator.getText() + gestellfachFuerTueteLocator.getText();
			
		
		}
		
		if(checkMaskeID(4221)) {
			
			
			tmp =getKommGestell() + zielGestellfachLokator.getText();
			tmp=custingGestellfach(tmp);
			
		}
		
		if(checkMaskeID(4203)) {
			
			
			tmp =getKommGestell() + zielGestellfachLokator.getText();
			//stmp=custingGestellfach(tmp);
		
		
		
		if (tmp.equals("-")) {
			System.out.println("Das ist wahrscheinlisch Wertebereich");
			return tmp;
		} 
		else {
			tmp=custingGestellfach(tmp);
		}
		}
		return tmp;
	}

	// Rückgabe Artikelnummer
	public String saveArtikelNr() {
		// System.out.println(artikelNrLocator.getText());
		return artikelNrLocator.getText();
	}
	
	// Custing Komm-Gestellfächer
	public String custingGestellfach(String tmp) {

		if (tmp.contains("-")) {
			tmp = tmp.replace("-", "");
			}
		if(standort.contains("BER")) {
			if (tmp.substring(0, 1).equals("2")) {
		
			tmp = tmp.replaceFirst("2", "6");
			}
		}
			tmp = tmp.substring(0, 4) + "0" + tmp.substring(4, tmp.length());
			System.out.println("Das ist aktuelle Gestellfachnummer: " + tmp);
			return tmp;
	}

	
	// Custing Touren-Gestellfächer
		public String custingTourenGestellfach(String tmp) {

			if (tmp.contains("-")) {
				tmp = tmp.replace("-", "");
				}
			if(standort.contains("BER")) {
				if (tmp.substring(0, 1).equals("2")) {
			
				tmp = tmp.replaceFirst("4", "8");
				}
			}
				tmp = tmp.substring(0, 4) + "0" + tmp.substring(4, tmp.length());
				System.out.println("Die Touren-Gestellfachnummer nach dem Custing: " + tmp);
				return tmp;
		}
	
	
	// Rückgabe Lagerplatz
	public String saveLagerplatzNr() {
		String LagerplatzID = null;
		String lagerplatz = lagerplatzLocator.getText();
		kommZone = getKommZone();
		if (kommZone.equals("Trocken")) {
			LagerplatzID = "01" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		} else if (kommZone.equals("Trocken_C")) {
			LagerplatzID = "01" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		} else if (kommZone.equals("Obst & Gemüse")) {
			LagerplatzID = "02" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		} else if (kommZone.equals("Mopro")) {
			LagerplatzID = "06" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		} else if (kommZone.equals("Tiefkuehl")) {
			LagerplatzID = "03" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		}

		else if (kommZone.equals("Brot und Backen")) {
			LagerplatzID = "09" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		}

		else if (kommZone.equals("Wertebereich")) {
			LagerplatzID = "08" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		} 
		else if (kommZone.equals("Obst & Gemüse Stück")) {
			LagerplatzID = "02" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		}
		else if (kommZone.equals("Getränke")) {
			LagerplatzID = "07" + lagerplatz.replace("-", "");
			System.out.println(LagerplatzID);
		}
		else {
			System.out.println("Lagerzone ist unbekannt.");
			LagerplatzID = "XX" + LagerplatzID;
		}
		
		
		
		
		return LagerplatzID;
	}

	// Rückgabe der zu pickender Menge
	public String saveMenge() {
		String tmp = mengenLocator.getText();
		String str[];
		str = tmp.split(" ");
		menge = str[0].toString();
		return menge;
	}

	public void setInfoGFachArtikelMengePlatz() {
		if (checkMaskeID(4201)) {
			//this.gestell = getKommGestellfach();
			this.artikel = saveArtikelNr();
			this.menge = saveMenge();
			this.lagerplatz = saveLagerplatzNr();

		} else {
			System.out.println("Falsche Maske");
		}
	}

	public String getKommGestell() {
		if (checkMaskeID(4203)) {
		gestell=zielGestellLokator.getText(); }
		
		return gestell;
	}

	public String getArtikel() {
		return artikel;
	}

	public String getMenge() {
		return menge;
	}

	public String getLagerplatz() {
		return lagerplatz;
	}

	public void picken() throws Throwable {
	
		// eventuell Supermarkt scannen 
		scanneSupermarkt();	

		// So lange picken, bis die Maske Drucker- oder Supermarkt-Scannen erreicht wurde
		while (!checkMaskeID(4204) && !checkMaskeID(4205) && !checkMaskeID(4200)  && !checkMaskeID(7303) && index!=15) {
		
			if(maske.equals(nextMaske)) {
				nextMaske=getMaskeID();
				index++;
				System.out.println("Index: "+index);
			}
			// Nulldurchgang und Eingabe einen Restmenge von 55 STK
			if (checkMaskeID(4010)) {
				inputText("55");
				clickOnOK();
			} 
			// Wenn der Komm_Platz besetzt ist, dann wird 5 Sec. gewartet und dann auf OK gekickt.
			kommPlatzBesetzt();
			
			// Implementierung OUG-Wiege im OST Kommissionierung 
			
			if( checkMaskeID(4261)) {
				
				inputText(tuetenPIDOrGestellLocator.getText());
			
				clickOnOK();
				
				inputText(custingGestellfach(getKommGestellfach()));
				clickOnOK();
	
				
			}
			
			
			if (checkMaskeID(4201)) {
				
				setInfoGFachArtikelMengePlatz();
				//if (!checkMaskeID(4216))
				inputText(getLagerplatz());
				clickOnOK();
			}
			if (checkMaskeID(4202) || checkMaskeID(4226)) {
				if (getMenge().equals("1")) {
					//inputText(getMenge());
					clickOnOK();
				} else {
					inputText(getMenge());
					clickOnOK();
				}
			}
			
			if (!checkMaskeID(4216) || !checkMaskeID(4010)) {
					inputText(getKommGestellfach());
					clickOnOK();
				}
			
			if (checkMaskeID(4215)) {
				String bdtQuellPlatz = bedienthekeQuellPlatzLokator.getText();
				bdtQuellPlatz = "05" + bdtQuellPlatz.replace("-", "");
				String lhmNUmmer = conToDB.getLHMNrvomQuellPlatzVonDB(bdtQuellPlatz, standort);
				System.out.println("Das ist die LHM-Nummer vom Platz: "+ lhmNUmmer);
				inputText(lhmNUmmer);
				clickOnOK();
			}

			if (checkMaskeID(4221)) {
							inputText(getKommGestellfach());
				clickOnOK();
			}
	
			
			
			}
		
			// // eventuell Supermarkt scannen 
			scanneSupermarkt();	
			
			
		
			maske=getMaskeID();
	}

	public void scanneSupermarkt(){
		if(checkMaskeID(4205)){
			String supermarkt=supermarktLocator.getText();
			supermarkt= supermarkt.replace("-", "");
			inputText(supermarkt);
			clickOnOK();
		}
	}
	public void setDruckerID(String id) throws InterruptedException {
		if(checkMaskeID(4204)){
		// Input Drucker ID
		inputText(id);
		// Bestätige mit OK
		clickOnOK();
		}
	}

	public void setLeerenKommGestell() {
		gestell=conToDB.getLeerKommGestellVonDB(standort);
		if (checkMaskeID(4200) && !(kommZone.equals("Wertebereich"))) {
			inputText(gestell);
			
			
		}
	
		if (checkMaskeID(4215) ) {
			inputText(gestell);
			
			
		}
	}

	public void clickAufWeiter() throws Throwable{
		if (checkMaskeID(9031)) {
			Thread.sleep(2000);
		}
		if (checkMaskeID(4109)) {
			weiterButtonLocator.click();
		}
		
	}
	
	


}

