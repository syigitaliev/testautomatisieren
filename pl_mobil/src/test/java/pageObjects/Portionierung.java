package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;
import de.bring.pl_mobil.ConnectToDB;
import de.bring.pl_mobil.TestBase;

public class Portionierung extends Base {
	String kommZone;
	String[] gestellArtikelMengePlatz = new String[10];
	String gestell;
	String artikel;
	String menge;
	String lagerplatz;
	TestBase tBase;
	ConnectToDB conToDB;

	// Initialisierung von Locators

		@FindBy(how = How.CLASS_NAME, using = "paramwinzig")
		private WebElement restMengen_Locator;

		@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td")
		private WebElement KommZoneLocator;
		
		@FindBy(how = How.XPATH, using = "//*[@id=\'uebersicht\']/table/tbody/tr[1]/td")
		private WebElement KommZoneGETLocator;


	// ____________________________________________________________________________________________
	// Definition der Konstruktors
	public Portionierung(WebDriver driver) {
		super(driver);
		tBase = new TestBase();
		conToDB = new ConnectToDB();
	}
	// ____________________________________________________________________________________________
	// Definition von Methoden



}
