package helper;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.model.Media;

import globals.Globals;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
public class Screenshot {
	
	public static String  makeScreenshot (WebDriver driver){
		
		String screenshotPath = Globals.EXTENTREPORT_IMAGES+ UUID.randomUUID()+".png";
		
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try{
			FileUtils.copyFile(screenshot, new File( Globals.EXTENTREPORT_FOLDER+screenshotPath));
		}catch (IOException e) {
			e.printStackTrace();
		}
		return screenshotPath;
	}	
	public static  MediaEntityModelProvider getScreenshot(WebDriver driver){
		MediaEntityModelProvider memp = new MediaEntityModelProvider(new Media());
		try {
			memp = MediaEntityBuilder.createScreenCaptureFromPath(makeScreenshot(driver)).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return memp;
		
	}
	
}

